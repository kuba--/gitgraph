package gitgraph

import (
	"context"
	"os"

	"github.com/cayleygraph/cayley/quad/nquads"
)

// Export exports quads in raw format
// Returns number of exported quads
func (g *GitGraph) Export(ctx context.Context) (int, error) {
	w := nquads.NewWriter(os.Stdout)
	defer w.Close()

	it, _ := g.store.QuadsAllIterator().Optimize()
	defer it.Close()

	n := 0
	for it.Next(ctx) {
		q := g.store.Quad(it.Result())
		if err := w.WriteQuad(q); err != nil {
			return 0, err
		}
		n++
	}

	return n, nil
}
