package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/kuba--/gitgraph"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "usage: %s [options]\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func main() {
	git := flag.String("git", "", "git directory")
	db := flag.String("db", ".", "database directory")
	flag.Parse()
	if *git == "" {
		flag.Usage()
	}

	g, err := gitgraph.Open(*db)
	if err != nil {
		log.Fatalln(err)
	}
	defer gitgraph.Close(g)

	ctx := context.TODO()
	if n, err := g.Import(ctx, *git); err != nil {
		log.Fatalln(err)
	} else {
		log.Printf("Imported: %d commits\n", n)
	}
}
