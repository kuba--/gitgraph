package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/kuba--/gitgraph"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "usage: %s [options]\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}
}

func main() {
	db := flag.String("db", ".", "database directory")
	limit := flag.Int("limit", 0, "top commits per git repository (0 means no limit)")
	sort := flag.String("sort", "touch", "sort commits by [add, remove, modify, touch, file]")
	nomerge := flag.Bool("nomerge", false, "do not show merge commits")
	flag.Parse()

	g, err := gitgraph.Open(*db)
	if err != nil {
		log.Fatalln(err)
	}
	defer gitgraph.Close(g)

	if *limit < 0 {
		*limit = 0
	}

	var by gitgraph.SortBy
	switch strings.ToLower(*sort) {
	case "add":
		by = func(cs1, cs2 *gitgraph.CommitStats) bool {
			return cs1.NumAdded > cs2.NumAdded
		}

	case "remove":
		by = func(cs1, cs2 *gitgraph.CommitStats) bool {
			return cs1.NumRemoved > cs2.NumRemoved
		}

	case "modify":
		by = func(cs1, cs2 *gitgraph.CommitStats) bool {
			return cs1.NumModified > cs2.NumModified
		}

	case "file":
		by = func(cs1, cs2 *gitgraph.CommitStats) bool {
			return cs1.NumFiles > cs2.NumFiles
		}

	case "touch":
		by = func(cs1, cs2 *gitgraph.CommitStats) bool {
			n1 := cs1.NumAdded + cs1.NumRemoved + cs1.NumModified
			n2 := cs2.NumAdded + cs2.NumRemoved + cs2.NumModified
			return n1 > n2
		}
	default:
		log.Println("Invalid -sort argument", *sort)
		flag.Usage()
	}

	ctx := context.TODO()
	if err := g.PrintStats(ctx, *limit, by, *nomerge); err != nil {
		log.Fatalln(err)
	}
}
